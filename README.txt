
      DRAN - Drupal Analysis Tool.
      Provided by www.vision-media.ca
      Developed by Tj Holowaychuk
      
      ------------------------------------------------------------------------------- 
      INSTALLATION
      -------------------------------------------------------------------------------
      
      - Simply locate at the root level of your Drupal installation (same level as index.php)
      - Assure that only you can execute dran, ex: $ chmod 0700 dran.sh 
        
      ------------------------------------------------------------------------------- 
      USAGE
      -------------------------------------------------------------------------------
      
      - For help simply enter the following command line: $ ./dran.sh -h
      - To quicky display all information use $ ./dran.sh -u http://example.com -va
      
      

