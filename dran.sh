#!/usr/bin/php -q
<?php

// @todo: support D6 and D5 in the program, detect version and change watchdog etc
// @todo: parse logs?
// @todo: update latest version of PHP CLI Framework

// Defaults
$_SERVER['HTTP_HOST']       = 'default';
$_SERVER['REMOTE_ADDR']     = '127.0.0.1';
$_SERVER['SERVER_SOFTWARE'] = 'PHP CLI';
$_SERVER['REQUEST_METHOD']  = 'GET';
$_SERVER['QUERY_STRING']    = '';
             
// Drupal bootstrap
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


// Set program options
// @todo: make -u optional within code
// @todo: make it so options with CLI:seto can have variations such as -h -? -help --help
CLI::seto(array(
    'u:' => 'Your website URI. Ex: -u http://vision-media.ca',
    'a'  => 'Load all statistics available (this may take some time, and will result in a large report, pipe into \'more\')',
    'p'  => 'Load performance related statistics.',
    's'  => 'Load system statistics such as node count, comment count etc.',
    'e'  => 'Load additional SEO related stats such as indexed pages, backlinks, etc.',
    'w'  => 'Load watchdog information.',
    'd'  => 'Load database statistics.',
    'l'  => 'Log output to files/logs/dran.log. When used via cron task -r should be used to truncate the log every so often, preventing bloating.',
    'r'  => 'Truncate log file (delete its contents).',
    'i'  => 'Load php common configuration information.',
    'v'  => 'Verbose output (output may not change for some options).',
    'h'  => 'Help.',
  ));   
   
// Display help
if (CLI::geto('h')){
  CLI::gethelp();
}
else {       
  Dran::init();          
}
        

/* -----------------------------------------------------------------

  Drupal Analysis 

------------------------------------------------------------------ */

class DRAN {
  
  /**
   * Site URI
   * 
   * @var string
   */
  static public $uri;  
   
  /**
   * Site URI components
   * 
   * @var array
   */
  static public $uri_components; 
  
  /**
   * IP Address
   * 
   * @var string
   */
  static public $ip;
  
  /**
   * Output sections
   * 
   * @var array
   */
  static public $output = array();
   
  /**
   * Initialize.
   */
  static public function init() {   
    $output = '';
    
    // Make sure URI is available            
    if ($uri = CLI::geto('u')){
      if (strstr($uri, 'http:')){
        self::$uri = $uri;      
        self::$uri_components = parse_url($uri);   
        self::$ip = self::get_ip(); 
      }
      else {
        CLI::error('Invalid URI, must begin with the http:// scheme.'); 
      }
    } 
    else {
      CLI::error('URI required, use the -h option for help.'); 
    } 
    
    // Sections
    self::_init_misc();
    self::_init_response();
    self::_init_system();
    self::_init_watchdog();
    self::_init_database();
    self::_init_metatags();
    self::_init_backlinks();
    self::_init_phpinfo();
    
    // Gather output
    $output = implode('', (array) self::$output); 
    
    // Log the output 
    if (CLI::geto('l')){
      self::log_set($output);  
    }
    
    // Truncate the log file
    if (CLI::geto('r')){
      if (self::log_empty()){
        $output .= "\n Log truncated. \n";
      }  
    }
        
    // Print final output
    echo $output;   
    exit(0);          
  }   
  
  /**
   * Initialize misc reports.
   */
  static protected function _init_misc() {
    if (CLI::geto('a') || CLI::geto('p')){
      $rows = array(); 
      
      $rows[] = array(                          
          'PHP Version',  
          phpversion(), 
        );     
      $rows[] = array(                          
          'IP Address',  
          self::$ip,  
        );
      $rows[] = array(                          
          'Last Cron Run',  
          date('M d Y', variable_get('cron_last', '')),  
        );
      $rows[] = array(                          
          'Page Rank',  
          self::get_pagerank(),  
        );

      self::$output[] = CLI::table($rows, array(), 'Misc');
    }
  }
  
  /**
   * Initialize backlink reports.
   */
  static protected function _init_backlinks() {
    if ((CLI::geto('a') || CLI::geto('e'))){
      $rows = array();
      $links = self::get_backlinks();
         
      if (count($links)){   
        foreach((array) $links AS $engine => $uri){
          foreach((array) $uri AS $uri => $data){
            $rows[] = array(
                $engine,
                strstr($uri, 'www.') ? 'www' : 'none',
                $data['total'],
              );
          }
        }
        
        self::$output[] = CLI::table($rows, array('Search Engine', 'Subdomain', 'Links'), 'Backlinks'); 
      }
    }
  }
  
  /**
   * Initialize response reports.
   */
  static protected function _init_response() {
    if ((CLI::geto('a') || CLI::geto('p')) && $response = self::get_response()){ 
      $rows = array();
      
      $rows[] = array(                          
          'Response Time',  
          $response->time,  
        );
      $rows[] = array(
          'Response Code',  
          $response->code,  
        ); 
      
      self::spacer($rows); 
        
      if (CLI::geto('v')){
        $rows[] = array(
            'Headers',  
            count($response->headers),  
          );                              
          
        foreach((array) $response->headers AS $header => $value){
          $rows[] = array(
              $header,  
              strlen($value) > 20 ? substr($value, 0, 20) . '...' : $value,  
            ); 
        }                    
      }   
        
      self::$output[] = CLI::table($rows, array(), 'Response'); 
    }
  }
  
  /**
   * Initialize metatags reports.
   */
  static protected function _init_metatags() {
    if ((CLI::geto('a') || CLI::geto('e'))){
      $rows = array();
      $metatags = @get_meta_tags(self::$uri);
      
      foreach((array) $metatags AS $tag => $value){
        $rows[] = array(
            $tag,
            strlen($value) > 20 ? substr($value, 0, 20) . '...' : $value,
          );
      }
        
      self::$output[] = CLI::table($rows, array(), 'Meta Tags'); 
    }
  }
  
  /**
   * Initialize watchdog reports.
   */
  static protected function _init_watchdog() {
    if ((CLI::geto('a') || CLI::geto('w'))){
      $rows = array();
      
      $watchdog = self::get_watchdog();
      
      foreach((array) $watchdog AS $severity => $count){
        $rows[] = array(
            self::format_severity($severity),
            $count,
          );
      }
        
      self::$output[] = CLI::table($rows, array(), 'Watchdog'); 
    }
  }
  
  /**
   * Initialize database reports.
   */
  static protected function _init_database() {
    if ((CLI::geto('a') || CLI::geto('d'))){
      global $db_url, $db_prefix, $db_type;
      $rows = array();  
      $parts = parse_url($db_url);
      
      $rows[] = array(
          'RDBMS ',
          $db_type,
        );
      $rows[] = array(
          'Version',
          db_version(),
        );
      $rows[] = array(
          'Host',
          $parts['host'],
        );
      $rows[] = array(
          'Database',
          trim($parts['path'], '/'),
        );
      $rows[] = array(
          'User',
          $parts['user'],
        );
        
      self::spacer($rows);
        
      if (CLI::geto('v')){
        // Status
        $status = self::get_database_status();   
        
        $rows[] = array(
            'System Status',
            '',
          );
          
        self::spacer($rows);
        
        foreach((array) $status AS $variable => $value){
          $rows[] = array(
              $variable,
              $value,
            );
        }
        
        self::spacer($rows);
        
        // System variables
        $variables = self::get_database_variables();   
        
        $rows[] = array(
            'System Variables',
            '',
          );
        
        foreach((array) $variables AS $section => $section_variables){
          self::spacer($rows);
          
          $rows[] = array(
              $section,
              '',
            );
          
          foreach((array) $section_variables AS $variable => $value){
            $rows[] = array(
                $variable,
                $value,
              );
          }
        }
      }  
              
      self::$output[] = CLI::table($rows, array(), 'Database'); 
    }
  }
  
  /**
   * Initialize php reports.
   */
  static protected function _init_phpinfo() { 
    if ((CLI::geto('a') || CLI::geto('i'))){
      global $db_url, $db_prefix, $db_type;
      $rows = array(); 
      $phpinfo = self::get_phpinfo();
      
      // Variables
      foreach((array) $phpinfo AS $variable => $value){
        $rows[] = array(
            $variable,
            $value,
          );  
      } 
      
      
      // Extensions
      if (CLI::geto('v')){
        self::spacer($rows);
        $extentions = @get_loaded_extensions();
        $rows[] = array(
            'Extensions Loaded',
            count($extentions),
          );  
        foreach((array) $extentions AS $extension){
          $rows[] = array(
              '',
              $extension,
            );  
        }
      }
      
      // Included files
      if (CLI::geto('v')){ 
        self::spacer($rows);
        $types = array();
        $included_files = @get_included_files();
        $rows[] = array(
            'Included Files',
            count($included_files),
          );  
        foreach((array) get_included_files() AS $filename){
          $ext = pathinfo($filename, PATHINFO_EXTENSION); 
          $types[$ext]++;
        }
        foreach((array) $types AS $type => $count){
          $rows[] = array(
              $type,
              $count,
            );  
        }
      }
      
      self::$output[] = CLI::table($rows, array(), 'PHP'); 
    }         
  }
  
  /**
   * Initialize system reports.
   */
  static protected function _init_system() {
    if ((CLI::geto('a') || CLI::geto('s')) && $system = self::get_system(CLI::geto('v'))){ 
      $rows = array();
      
      // Nodes
      $rows[] = array(                          
          'Nodes',  
          $system->node_count,  
        );     
      // Nodes - verbose
      if (CLI::geto('v')){
        foreach((array) $system->node_types AS $type => $count){
          $rows[] = array(
              '  ' . $type,  
              $count,  
            );  
        }
      } 
      
      self::spacer($rows);
      
      // Users
      $rows[] = array(                          
          'Users',  
          $system->user_count - 1,  
        );
      // Users - verbose
      if (CLI::geto('v')){
        foreach((array) $system->user_roles AS $role => $count){
          if (!strlen($role)){
            continue;
          }
          
          $rows[] = array(
              '  ' . $role,  
              $count,  
            );  
        }
      } 
      
      self::spacer($rows); 
        
      // Comments
      $rows[] = array(
          'Comments',  
          $system->comment_count,  
        );     
      
      self::spacer($rows); 
      
      // Modules
      $rows[] = array(
          'Modules Enabled',  
          $system->module_count,  
        );     
      // Modules - verbose
      if (CLI::geto('v')){
        foreach((array) $system->modules AS $module => $throttle){
          $rows[] = array(
              '  ' . $module,  
              $throttle,  
            );  
        }  
      }
      
      self::spacer($rows); 
      
      // Sessions
      $rows[] = array(
          'Sessions',  
          $system->session_count,  
        );  
      // Sessions - verbose
      if (CLI::geto('v')){
        foreach((array) $system->sessions AS $type => $count){
          $rows[] = array(
              '  ' . $type,  
              $count,  
            );  
        }  
      }
      
      self::spacer($rows); 
         
      // Terms
      $rows[] = array(
          'Terms',  
          $system->term_count,  
        );   
      
      self::spacer($rows); 
             
      // Aliases
      $rows[] = array(
          'Aliases',  
          $system->alias_count,  
        );   
      
      self::$output[] = CLI::table($rows, array(), 'System'); 
    }
  }
  
  /**
   * Format watchdog severity.
   * 
   * @param int $severity
   * 
   * @return string
   */
  static public function format_severity($severity) {
    switch($severity){
      case 0:
        return 'Notice';
        break;
        
      case 1:
        return 'Warning';
        break;
        
      case 2:
        return 'Error';
        break;
    }
  }
   
  /**
   * Get IP Address.
   * 
   * @return string
   * 
   * @todo: use the hostname, not $uri
   */
  static public function get_ip() {
    list($ip) = gethostbynamel(str_replace('http://', '', self::$uri_components['host']));  
    return $ip; 
  }  
                                
  /**
   * Get watchdog rows.
   * 
   * @return array
   */
  static public function get_watchdog() {
    $output = array();
    
    $results = db_query("SELECT COUNT(*) as count, severity FROM {watchdog} GROUP BY severity");
    while($result = db_fetch_array($results)){
      $output[$result['severity']] = $result['count'];
    }
    
    return $output;
  }
  
  /**
   * Get database benchmarks.
   * 
   * @return array
   */
  static public function get_database_benchmarks() {
    $output = array();
    
    // @todo:
    
    return $output;
  }
  
  /**  
   * Get database status information.
   * 
   * @return array
   */
  static public function get_database_status() {
    $output = array();
    
    $results = db_query("SHOW STATUS WHERE  
        variable_name LIKE 'aborted_%'
        OR variable_name LIKE 'max_used_connections'   
        OR variable_name LIKE 'connections'
        OR variable_name LIKE 'select_full_join'
        OR variable_name LIKE 'select_scan'
        OR variable_name LIKE 'slow_queries'
        OR variable_name LIKE 'table_locks_waited' 
        OR variable_name LIKE 'open_tables' 
        OR variable_name LIKE 'opened_tables' 
        OR variable_name LIKE 'opened_tables' 
        OR variable_name LIKE 'Qcache_%'
        OR variable_name LIKE 'create_%'
        OR variable_name LIKE 'key_%'  
      ");
                               
    while($result = db_fetch_array($results)){
      $output[$result['Variable_name']] = $result['Value'];
    }
    
    return $output;
  }
  
  /**
   * Get database variables; nested within 'sections' such as cache.
   * 
   * @return array
   *   - section
   *     - variables
   */
  static public function get_database_variables() {
    $output = array();
    
    // Query cache
    $results = db_query("SHOW VARIABLES WHERE variable_name LIKE 'query_cache%'");
    while($result = db_fetch_array($results)){
      $output['Query Cache'][$result['Variable_name']] = $result['Value'];
    }
    
    // Table related
    $results = db_query("SHOW VARIABLES WHERE variable_name LIKE 'table%'");
    while($result = db_fetch_array($results)){  
      $output['Table'][$result['Variable_name']] = $result['Value'];
    }
    
    // Key related
    $results = db_query("SHOW VARIABLES WHERE variable_name LIKE '%key%'");
    while($result = db_fetch_array($results)){
      $output['Keys'][$result['Variable_name']] = $result['Value'];
    }
    
    // Tmp related
    $results = db_query("SHOW VARIABLES WHERE variable_name LIKE 'tmp_%'");
    while($result = db_fetch_array($results)){
      $output['Temp'][$result['Variable_name']] = $result['Value'];
    }
       
    // Log related
    $results = db_query("SHOW VARIABLES WHERE variable_name LIKE 'log_%' OR variable_name LIKE 'long_%'");
    while($result = db_fetch_array($results)){
      $output['Log'][$result['Variable_name']] = $result['Value'];
    }
    
    // Max vars
    $results = db_query("SHOW VARIABLES WHERE variable_name LIKE 'max_%'");
    while($result = db_fetch_array($results)){
      $output['Max'][$result['Variable_name']] = $result['Value'];
    }
       
    return $output;
  }
  
  /**
   * Get common PHP conf information.
   * 
   * @return array
   *   Assoc array of php vars and their values.
   * 
   * @todo: finish
   */
  static public function get_phpinfo() {
    return array(  
        'memory_limit' => ini_get('memory_limit'), 
        'post_max_size' => ini_get('post_max_size'), 
        'upload_max_filesize' => ini_get('upload_max_filesize'), 
      );     
  }
  
  /**
   * Get response object.
   * 
   * @return object
   */
  static public function get_response() {
    
    $start = microtime(TRUE);           
    $response = drupal_http_request(self::$uri);     
    $stop = microtime(TRUE); 
    
    $response->time = round($stop - $start, 2); 
    
    return $response;
  }
  
  /**
   * Get Google Page Rank.
   * 
   * @return mixed
   *   - success: Pagerank integer
   *   - failure: String indicating that the limit has exceeded
   */
  static public function get_pagerank() {
    $response = drupal_http_request('http://www.pageranktool.net/google_pr.php?url=' . self::$uri . '&query=Query', array(), 'GET', NULL, 1);

    if ($response->data){
      // @todo: make use of more than one result.
      // @todo: check if banned from the site and then log watchdog messages OR fallback to other PR sites.
      preg_match('/\<td\>toolbarqueries.google.com\<\/td\>\<td align=\'center\'\>([^<]+)\<\/td\>/i', $response->data, $matches);
                   
      if (is_numeric($matches[1])){
        return $matches[1];
      }
    }
  
    return 'Limit Exceeded.'; 
  }
  
  /**
   * Get backlinks.
   * 
   * @return array
   *   - engine
   *      - uri
   *       - total
   *         - count
   */
  static public function get_backlinks() {
    $links = array();
    
    // Remove or add www. for the alternate uri
    $alternate_uri = strstr('www.', self::$uri) ? str_replace('www.', '', self::$uri) : str_replace('http://', 'http://www.', self::$uri);
    
    self::_get_backlinks_google($links, self::$uri);
    self::_get_backlinks_yahoo($links, self::$uri);
    self::_get_backlinks_yahoo($links, $alternate_uri);
    self::_get_backlinks_yahoo($links, $alternate_uri);
     
    return $links;
  }

  /**
   * Google link stats.
   */
  static protected function _get_backlinks_google(&$links, $uri) {
    $response = drupal_http_request('http://www.google.com/search?hl=en&q=link%3A' . urlencode($uri) . '&btnG=Search'); 
    
    // Markup on these pages are frequently changing so our regex is quick and dirty to get the results
    // This will most likely need to be update if not working properly                             
    if (preg_match('/\<b\>([^<]+)\<\/b\> linking to/i', $response->data, $matches)){    
      $links['google'][$uri]['total'] = $matches[1];   
    }
  }

  /**
   * Yahoo link stats.
   */
  static protected function _get_backlinks_yahoo(&$index, $uri) {
    $response = drupal_http_request('http://siteexplorer.search.yahoo.com/search?p=' . urlencode($uri) . '&bwm=i&bwmf=u&bwms=p&fr2=seo-rd-se'); 
    
    // Markup on these pages are frequently changing so our regex is quick and dirty to get the results  
    // This will most likely need to be update if not working properly                                           
    if (preg_match('/of about.*?<span.*?>(.*?)\<\/span\>/i', $response->data, $matches)){
      $index['yahoo'][$uri]['total'] = $matches[1]; 
    }
  }

  /**
   * Get system data.
   * 
   * @param bool $details
   * 
   * @return object
   */
  static public function get_system($details = FALSE) {
    $system = new stdClass();                  
    
    $system->node_count = db_result(db_query("SELECT COUNT(*) FROM {node}"));
    $system->user_count = db_result(db_query("SELECT COUNT(*) FROM {users}"));
    $system->comment_count = db_result(db_query("SELECT COUNT(*) FROM {comments}"));
    $system->module_count = db_result(db_query("SELECT COUNT(*) FROM {system} WHERE status = 1 AND type = 'module'"));
    $system->term_count = db_result(db_query("SELECT COUNT(*) FROM {term_data}"));
    $system->alias_count = db_result(db_query("SELECT COUNT(*) FROM {url_alias}"));
    $system->session_count = db_result(db_query("SELECT COUNT(*) FROM {sessions}"));
    
    if ($details){
      // Node types and counts
      $system->node_types = array();
      $results = db_query("SELECT type, COUNT(*) as count FROM node GROUP BY type");
      while($result = db_fetch_array($results)){
        $system->node_types[$result['type']] = $result['count'];   
      }
      
      // User roles and counts
      $system->user_roles = array();
      $results = db_query("SELECT count(*) as count, ur.name as role FROM {users} u LEFT JOIN {users_roles} r ON u.uid = r.uid LEFT JOIN {role} ur ON ur.rid = r.rid GROUP BY r.rid");
      while($result = db_fetch_array($results)){
        $system->user_roles[$result['role']] = $result['count'];   
      }
      
      // Modules and throttle status
      $system->modules = array();
      $results = db_query("SELECT name, throttle FROM {system} WHERE status = 1 AND type = 'module'");
      while($result = db_fetch_array($results)){
        $system->modules[$result['name']] = $result['throttle'];   
      }
      
      // Session thresholds and counts
      $system->sessions = array();
      $system->sessions['24 hours'] = db_result(db_query("SELECT COUNT(*) FROM {sessions} WHERE timestamp > '%s'", strtotime('-1 day')));
      $system->sessions['12 hours'] = db_result(db_query("SELECT COUNT(*) FROM {sessions} WHERE timestamp > '%s'", strtotime('-12 hours')));
      $system->sessions['1 hours'] = db_result(db_query("SELECT COUNT(*) FROM {sessions} WHERE timestamp > '%s'", strtotime('-1 hours')));
      $system->sessions['15 minutes'] = db_result(db_query("SELECT COUNT(*) FROM {sessions} WHERE timestamp > '%s'", time() - 900));
    }
    
    return $system;
  }
  
  /**
   * Log content.
   * 
   * @param string $content
   */                                                                
  public static function log_set($content) { 
    if ($dest = file_create_path('logs')){ 
      if (file_check_directory($dest, FILE_CREATE_DIRECTORY)){ 
        $filename = $dest . '/dran.log';
        
        // Create log header
        $content = "\n[Log-" . date('d M Y') . "]\n" . $content;
        
        if (file_exists($filename) && is_writeable($filename)){     
          // Append $content
          $fh = fopen($filename, 'ab');
          fwrite($fh, $content);
          fclose($fh);
        }
        else {
          // Create file with $content
          file_save_data($content, $filename, FILE_EXISTS_REPLACE);  
        }
      }
    } 
  }
  
  /**
   * Empty the log.
   * 
   * @return bool
   */
  public static function log_empty() {
    if ($fh = fopen(file_directory_path() . '/logs/dran.log', 'wb')){
      fwrite($fh, '');
      fclose($fh);   
      
      return TRUE;
    }
  
    return FALSE;
  }
  
  /**
   * When in verbose mode add a row spacer for readability.
   * 
   * @param array $rows
   *   Row array passed by reference.
   * 
   * @param int $cols
   *   Number of columns used in the table.
   */
  public static function spacer(&$rows, $cols = 2) {
    if (CLI::geto('v')){
      $rows[] = array_pad(array(), $cols, ' ');;   
    }  
  }
}

/* -----------------------------------------------------------------

  CLI 

------------------------------------------------------------------ */
   
/**
 * PHP CLI
 *
 * An open source command line interface framework for PHP 4.3.0 or newer.
 *
 * @package  PHP CLI    
 * @author  Tj Holowaychuk <tj@vision-media.ca>  
 * @copyright  Copyright (c) 2008 Tj Holowaychuk  
 * @link  http://cliframework.com  
 * @since Version 1.0
 */
 
/**
 * Bullet Lists.
 */
define('CLI_LIST_BULLET',  1);
define('CLI_LIST_NUMERIC', 2);
define('CLI_LIST_ALPHA',   3);
 
class CLI {
  
  /**
   * Options.
   * 
   * @var array  
   * 
   * @access public 
   * 
   * @since Version 1.0
   */
  static public $o;
  
  /**
   * Option descriptions.
   * 
   * @var array 
   * 
   * @access public  
   *  
   * @since Version 1.0 
   */
  static public $od;
  
  /* -----------------------------------------------------------------
  
    Methods 
  
  ------------------------------------------------------------------ */
  
  /**
   * Set options.
   * 
   * @param array $options
   *   Array of assoc options; option => description
   *     - Optional: 'v'
   *     - Requires value: 'd:'
   *     - Optional value: 'h::'; 
   *         - Note: only supported in PHP 5.3.x and greater
   * 
   * Note: The register_argc_argv option must be enabled for this method to work. 
   * 
   * @access public
   * 
   * @since Version 1.0
   * 
   * @todo support long options 
   * @todo format help better 
   */
  static public function seto($options) {       
    self::$od = $options;
    self::$o =  getopt((string) implode((array) array_keys($options)));
  }
  
  /**
   * Get option.
   * 
   * @param string $option
   * 
   * @return mixed
   *   - Option set without value required: TRUE
   *   - Option set value: Value returned
   *   - Required option set with value: Value returned
   *   - Option not set or value is not present: FALSE 
   * 
   * @access public 
   * 
   * @since Version 1.0
   */
  static public function geto($option) {  
    if (isset(self::$o[$option]) && $value = self::$o[$option]){
      return $value;  
    }
        
    return array_key_exists($option, self::$o);
  }
 
  /**
   * Get an argument value by index.
   * 
   * @param int $i
   * 
   * @return mixed
   *   - success: String 
   *   - failure: FALSE
   * 
   * @access public
   * 
   * @since Version 1.0 
   * 
   * @todo check if the argument is an option, or how to handle this better
   */
  static public function getv($i) {  
    global $argv; 
    return (!empty($argv[$i])) ? $argv[$i] : FALSE;
  }
  
  /**
   * Display help information.
   * 
   * @access public 
   * 
   * @since Version 1.0
   * 
   * @todo: hook for additional help, or global
   * @todo: support php 5.30's :: optional values
   */
  static public function gethelp() {
    echo "\n Options ---------------------------------------- \n";
    
    foreach((array) self::$od AS $option => $description){
      if (strstr($option, ':')){
        echo "\n  -" . str_replace(':', '', $option) . " : {$description}";
      }
      else {
        echo "\n  -" . $option . " : {$description}";      
      }
    }
              
     echo "\n\n ------------------------------------------------ \n\n";
  }
  
  /**
   * Table output.
   * 
   * @param array $rows
   * 
   * @param array $headers 
   *   (optional) Column headers.
   * 
   * @param string $caption
   *   (optional) Table caption.
   * 
   * @param $pad_type_headers
   *   (optional) Padding method.
   *   - STR_PAD_LEFT
   *   - STR_PAD_RIGHT
   *   - STR_PAD_BOTH
   * 
   * @param $pad_type_cells
   *   (optional) Padding method.  
   *   - STR_PAD_LEFT
   *   - STR_PAD_RIGHT
   *   - STR_PAD_BOTH
   * 
   * @return string
   * 
   * @access public 
   * 
   * @since Version 1.0
   * 
   * @todo support and detect colspans
   */
  static public function table($rows, $headers = array(), $caption = NULL, $pad_type_headers = STR_PAD_BOTH, $pad_type_cells = STR_PAD_RIGHT) {
    $output = '';
    $cl = array();                                                          
    $clp = 0;    
    
    // Rows are manditory for the table to print
    if (!count($rows)){
      return FALSE;
    }                                                      
    
    // Determine each columns length
    foreach((array) $headers AS $i => $header){
      $cl[$i] = strlen($header) > $cl[$i] ? strlen($header) : $cl[$i];
    }
    foreach((array) $rows AS $row){
      foreach((array) $row AS $i => $cell){
        $cl[$i] = strlen($cell) > $cl[$i] ? strlen($cell) : $cl[$i];
      }
    }
          
    // Column length product
    foreach((array) $cl AS $l){
      $clp += $l;
    }     
          
    // Caption
    if ($caption){
      $output .= "\n  ";                                     
      $output .= str_pad(" {$caption} ", $clp + ((count($cl) * 2) + 2), '-', STR_PAD_BOTH);
    }
    // Headers
    if (count($headers)){
      $output .= "\n | ";
      foreach((array) $headers AS $i => $header){
        $output .= str_pad($header, $cl[$i], ' ', $pad_type_headers) . " | ";
      }   
    }  
    // Rows
    foreach((array) $rows AS $row){
      $output .= "\n | ";
      foreach((array) $row AS $i => $cell){      
        $output .= str_pad($cell, $cl[$i], ' ', $pad_type_cells) . " | ";   
      }
    }
    $output .= "\n";
    
    return $output;
  }
  
  /**
   * List.
   * 
   * @param array $items
   * 
   * @param array $caption 
   *   (optional) List caption.
   * 
   * @param int $list_type
   *   (optional) List style type.
   *      - CLI_LIST_ALPHA   
   *      - CLI_LIST_BULLET  
   *      - CLI_LIST_NUMERIC
   * 
   * @return string
   * 
   * @access public 
   * 
   * @since Version 1.0
   */
   static public function item_list($items, $caption = NULL, $list_type = CLI_LIST_BULLET){
     $output = "\n";
     
     // Caption
     if ($caption){
       $output .= " {$caption} \n";
     }
     
     // List items
     $output .= self::_item_list($items, $list_type);
     
     return $output;  
   }
   
   /**
   * Item list helper.
   * 
   * Loop menu items recursively to generate 
   * a item list formated via $list_type 
   * 
   * @see self::item_list
   * 
   * @access protected 
   * 
   * @since Version 1.0
   */
   static protected function _item_list($items, $list_type = CLI_LIST_BULLET, $depth = 0) {
     $output = '';
     $list_id = md5((string) time());
     
     foreach((array) $items AS $i => $item){
       if (is_array($item)){
         $output .= self::_item_list($item, $list_type, $depth + 1);
       }
       else {        
         $output .= str_repeat('  ', $depth + 2) . self::_item_list_type($i, $list_id, $list_type, $depth) . ' ' . $item . " \n"; 
       }
     }
     
     return $output;
   }
   
   /**
   * Item list type helper.
   * 
   * Based on the current depth of an item list
   * return the proper bullet format based on
   * the $list_type pram
   * 
   * @see self::_item_list
   * 
   * @access protected 
   * 
   * @since Version 1.0
   */
   static protected function _item_list_type($i, $list_id, $list_type = CLI_LIST_BULLET, $depth = 0) {
     static $lists;
     
     $lists[$list_id][$depth]++;
     
     switch($list_type){
       case CLI_LIST_ALPHA:
         $output = !$depth ? chr(96 + (int) $lists[$list_id][$depth]) : '-'; 
         break;
        
       case CLI_LIST_BULLET:
         $output = '-';
         break;
       
       case CLI_LIST_NUMERIC:
         $output = !$depth ? (int) $lists[$list_id][$depth] : chr(96 + (int) $lists[$list_id][$depth]);  
         break;
     }
     
     return $output . '.';
   }
    
  /**
   * Prompt user input.
   * 
   * @param string $message   
   * 
   * @return string
   *   Response.
   * 
   * @access public 
   * 
   * @since Version 1.0
   */
  static public function prompt($message) {
    echo $message . " \n";
    $fp = fopen("/dev/stdin", "r");
    $input = fgets($fp, 255);
    fclose($fp);
    return $input;
  }
                                                  
  /**
   * Output an error message, halting the program.
   * 
   * @param string $message
   * 
   * @access public 
   * 
   * @since Version 1.0
   * 
   * @todo: support errorno's
   * @todo: errorno map
   */
  static public function error($message) {
    $stderr = fopen('php://stderr', 'w');
    fwrite($stderr, "\nError: {$message}\n");
    fclose($stderr); 
    exit;
  }

  /**
   * Debug.
   * 
   * @param mixed $var
   * 
   * @access public 
   * 
   * @since Version 1.0
   */
  static public function dbg($var) {
    echo "\n" . print_r($var) . "\n";  
  }  
}                     
?> 